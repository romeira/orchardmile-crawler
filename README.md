## Maison Margiela Web Crawler
Orchard Mile's test

### Install
```sh
pipenv install
```

### Run
```sh
pipenv run scrapy crawl maison-margiela
```

### Deploy to Scrapy Cloud
```sh
shub deploy <project>
```
