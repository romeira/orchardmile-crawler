# -*- coding: utf-8 -*-

from scrapy import Field, Item


class Product(Item):
    """
    The output of the spider should be an item 
    for each product page on the site
    """
    brandId = Field() # the static string "maison-margiela"
    url = Field() # the URL of the product page being scraped
    name = Field() # the product's name
    description = Field() # the product's description
    images = Field() # a list of the URLs of images of the product itself
    color = Field() # color of the product, if it listed on the page, or None
    variants = Field() # a list of subdocuments, one for each size
