# -*- coding: utf-8 -*-

import json

from jmespath import search as jsearch
from scrapy.http import Request
from scrapy.linkextractors import LinkExtractor
from scrapy.spiders import CrawlSpider, Rule

from orchardmile.loaders import MaisonMargielaLoader

from .constants import URL
from .constants import XPATH, CSS, JMES
from .constants import REGEX


class MaisonMargielaSpider(CrawlSpider):
    name = 'maison-margiela'
    allowed_domains = ['maisonmargiela.com']

    def __init__(self, search=False, *a, **kw):
        super(MaisonMargielaSpider, self).__init__(*a, **kw)

        self.search = search and search.lower() != 'false'
        self.start_urls = [URL['search' if self.search else 'crawl']]


    rules = (
        Rule(LinkExtractor(
                allow=REGEX['allow'],
                restrict_xpaths=XPATH['restrict']),
            callback='parse_listing',
        ),
    )


    def parse(self, response):
        if self.search:
            return self.parse_listing(response)
        else:
            return super(MaisonMargielaSpider, self).parse(response)


    def parse_listing(self, response):
        codes = response.css(CSS['codes']).extract()
        for code in codes:
            url = URL['variants'](code)
            yield Request(url, callback=self.parse_variants)


    def parse_variants(self, response):
        data = json.loads(response.body)
        colors = jsearch(JMES['colors'], data)
        sizes = jsearch(JMES['sizes'], data)

        for color_id, color, link in colors:
            variants = []
            for size_id, size in sizes:
                stock = jsearch(JMES['stock'](color_id, size_id), data)
                variants.append({
                    'color': color,
                    'size': size,
                    'stock': stock,
                })

            meta = {'color': color, 'variants': variants}
            yield Request(link, meta=meta, callback=self.parse_item)


    def parse_item(self, response):
        item_section = response.css(CSS['item_section'])
        price = float(item_section.css(CSS['price']).extract_first())

        loader = MaisonMargielaLoader(selector=item_section, price=price)
        loader.add_value('brandId', self.name)
        loader.add_value('url', response.url)
        loader.add_css('name', CSS['name'])
        loader.add_css('description', CSS['description'])
        loader.add_css('images', CSS['images'], re=REGEX['images'])
        loader.add_value('color', response.meta['color'])
        loader.add_value('variants', response.meta['variants'])
        item = loader.load_item()

        yield item
