# -*- coding: utf-8 -*-

URL = {
    'crawl': 'https://www.maisonmargiela.com/us',

    'search': ('https://www.maisonmargiela.com/Search/'
               'Index?textsearch=&siteCode=MMMARGIELA_US&'
               'season=&department=&gender=D'),

    'variants': ('https://www.maisonmargiela.com/yTos/api/Plugins/'
                 'ItemPluginApi/GetCombinationsAsync/?'
                 'siteCode=MMMARGIELA_US&code10={}').format,
}

XPATH = {
    # Rules
    'restrict': '//*[@data-target-gender="D"]',
}

CSS = {
    # Listing
    'codes': 'ul.products > li::attr(data-ytos-item)',

    # Item
    'item_section': 'section.itemTop',
    'price': 'span.price > span.value::attr(data-ytos-price)',
    'name': '#itemModelName span',
    'description': 'div.editorialdescription',
    'images': 'img::attr(srcset)',
}

JMES = {
    # Variants
    'colors': 'ColorsFull[].[ColorId, Description, Link]',
    'sizes': 'SizesFull[].[Size, Description]',
    'stock': ('ModelColorSizes[?'
                'Color.ColorId == `{}` && Size.Size == `{}`'
              '].Quantity | [0]').format,
}

REGEX = {
    # Item
    'images': r'([^, ]+)[^,]*$',

    # Rules
    'allow': r'/(women|unisex)/',
}
