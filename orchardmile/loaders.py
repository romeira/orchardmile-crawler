# -*- coding: utf-8 -*-

from scrapy.loader import ItemLoader
from scrapy.loader.processors import Identity, Join, TakeFirst
from scrapy.loader.processors import MapCompose
from w3lib.html import remove_tags

from orchardmile.items import Product


def variant_processor(variant, loader_context):
    variant['price'] = loader_context.get('price')

    size = variant.get('size')
    if not size or size.lower() == 'onesize':
        variant['size'] = None

    variant['stock'] = variant.get('stock', 0)

    return variant


class MaisonMargielaLoader(ItemLoader):
    default_item_class = Product
    default_input_processor = MapCompose(remove_tags, str.strip)
    default_output_processor = TakeFirst()

    description_out = Join()
    images_out = Identity()
    variants_in = MapCompose(variant_processor)
    variants_out = Identity()
